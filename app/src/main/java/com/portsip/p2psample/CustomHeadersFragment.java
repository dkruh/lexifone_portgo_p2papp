package com.portsip.p2psample;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.portsip.PortSipSdk;
import com.portsip.p2psample.util.CustomHeaders;
import com.portsip.p2psample.util.SettingConfig;

import android.widget.AdapterView.OnItemSelectedListener;

/**
 * Created by DoronK on 6/16/2015.
 */
public class CustomHeadersFragment extends Fragment implements OnItemSelectedListener, View.OnClickListener {

    PortSipSdk mSipSdk;
    Context context = null;
    private String nativeLanguage, foreignLanguage;
    private String[] languages;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        mSipSdk = ((P2pApplication) getActivity().getApplicationContext())
                .getPortSIPSDK();
        View rootView = inflater.inflate(R.layout.customheaderview, null);
        initView(rootView);
        return rootView;
    }

    private void initView(View view) {
        loadCustomHeaders(view);
        view.findViewById(R.id.btonSave).setOnClickListener(this);
    }

    private void saveCustomHeaders(View view) {
        CustomHeaders customHeaders = new CustomHeaders();
        customHeaders.setNativeLanguage(nativeLanguage);
        customHeaders.setForeignLanguage(foreignLanguage);
        customHeaders.setBridgeId(((EditText) view.findViewById(R.id.etbrdigeId)).getText().toString());
        customHeaders.setContactUser(((EditText) view.findViewById(R.id.etcontactUser)).getText().toString());
        customHeaders.setGlobalSessionId(((EditText) view.findViewById(R.id.etglobalSessionId)).getText().toString());
        customHeaders.setTranslated(((CheckBox) view.findViewById(R.id.cbisTranslated)).isChecked());
        customHeaders.setStartInMute(((CheckBox) view.findViewById(R.id.cbstartInMute)).isChecked());
        customHeaders.setMainLeg(((CheckBox)view.findViewById(R.id.cbmainUser)).isChecked());
        SettingConfig.setCustomHeaders(context, customHeaders);
    }

    private void loadCustomHeaders(View view) {
        CustomHeaders customHeaders = SettingConfig.getCustomHeaders(context);
        String item = customHeaders.getNativeLanguage() == null ? "" : customHeaders.getNativeLanguage();
        Spinner spNativeLang = (Spinner) view.findViewById(R.id.spnativeLang);
        if (languages == null) {
            languages = getResources().getStringArray(
                    R.array.lang);
        }
        spNativeLang.setAdapter(new ArrayAdapter<String>(context,
                R.layout.viewspinneritem, languages));
        spNativeLang.setOnItemSelectedListener(this);
        spNativeLang.setSelection(getLanguagesSelectionIndex(languages, item));
        item = customHeaders.getForeignLanguage() == null ? "" : customHeaders.getForeignLanguage();
        Spinner spForeigneLang = (Spinner) view.findViewById(R.id.spforeigneLang);
        spForeigneLang.setAdapter(new ArrayAdapter<String>(context,
                R.layout.viewspinneritem, languages));

        spForeigneLang.setOnItemSelectedListener(this);
        spForeigneLang.setSelection(getLanguagesSelectionIndex(languages, item));
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.cbisTranslated);
        checkBox.setChecked(customHeaders.isTranslated());
        checkBox = (CheckBox) view.findViewById(R.id.cbstartInMute);
        checkBox.setChecked(customHeaders.isStartInMute());

        EditText editText = (EditText) view.findViewById(R.id.etbrdigeId);
        editText.setText(customHeaders.getBridgeId());
        editText = (EditText) view.findViewById(R.id.etcontactUser);
        editText.setText(customHeaders.getContactUser());

        checkBox = (CheckBox) view.findViewById(R.id.cbmainUser);
        checkBox.setChecked(customHeaders.isMainLeg());

    }

    private int getLanguagesSelectionIndex(String[] languages, String item) {
        if (item == null) {
            return -1;
        }
        int index = 0;
        for (String lang : languages) {
            if (lang.equals(item)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position >= 0) {
            switch (parent.getId()) {
                case R.id.spnativeLang:
                    nativeLanguage = languages[position];
                    break;
                case R.id.spforeigneLang:
                    foreignLanguage = languages[position];
                default:
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        saveCustomHeaders(v.getRootView());
    }
}
