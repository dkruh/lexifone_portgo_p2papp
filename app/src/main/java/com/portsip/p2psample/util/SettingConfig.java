package com.portsip.p2psample.util;

import java.util.HashMap;

import com.portsip.PortSipEnumDefine;
import com.portsip.PortSipSdk;
import com.portsip.p2psample.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;

public class SettingConfig {
    public static final String PRE_NAME = "com.portsip.p2psample_preferences";
    static final String PORTSIP_USERNAME = "PORTSIP_USERNAME";
    static final String PORTSIP_USERPASSWORD = "PORTSIP_USEPWD";
    static final String PORTSIP_LOCAL_IP = "PORTSIP_LOCAL_IP";
    static final String PORTSIP_LOCAL_PORT = "PORTSIP_LOCAL_PORT";
    static final String PORTSIP_SERVER_IP = "PORTSIP_SERVER_IP";

    static final String PORTSIP_USEDISPALYNAME = "PORTSIP_DISNAME";
    static final String PORTSIP_SRTPTYPE = "PORTSIP_SRTPTYPE";
    static final String PORTSIP_NATIVELANG = "PORTSIP_NATIVELANG";
    static final String  PORTSIP_GLOBALID =  "PORTSIP_GLOBALID";
    static final String PORTSIP_FOREIGNLANG = "PORTSIP_FORIEGNLANG";
    static final String PORTSIP_BRDIGEID = "PORTSIP_BRIDGEID";
    static final String PORTSIP_BRDIGECONTACT = "PORTSIP_BRIDGECONTECT";
    static final String PORTSIP_ISTRANSLATED = "PORTSIP_ISTRANSLATED";
    static final String PORTSIP_STARTINMUTE = "PORTSIP_STARTINMUTE";
    static final String PORTSIP_MAINLEG = "PORTSIP_MAINLEG";
     static final String  PORTSIP_LASTDIALEDNUMBER =  "PORTSIP_LASTDIALNUMBER";


    public static HashMap<Integer, Boolean> getCodecs(Context context) {
        HashMap<Integer, Boolean> codecs = new HashMap<Integer, Boolean>();
        SharedPreferences mPrefrence = context.getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);

        String key;
        boolean value;
        Resources resources = context.getResources();
        //g729
        key = resources.getString(R.string.MEDIA_G729);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_G729, value);
        //pcma
        key = resources.getString(R.string.MEDIA_PCMA);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_PCMA, value);
        //pcmu
        key = resources.getString(R.string.MEDIA_PCMU);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_PCMU, value);
        //gsm
        key = resources.getString(R.string.MEDIA_GSM);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_GSM, value);
        //g722
        key = resources.getString(R.string.MEDIA_G722);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_G722, value);
        //ilbc
        key = resources.getString(R.string.MEDIA_ILBC);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_ILBC, value);
        //amr
        key = resources.getString(R.string.MEDIA_AMR);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_AMR, value);
        //amrwb
        key = resources.getString(R.string.MEDIA_AMRWB);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_AMRWB, value);
        //speex
        key = resources.getString(R.string.MEDIA_SPEEX);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_SPEEX, value);
        //speexwb
        key = resources.getString(R.string.MEDIA_SPEEXWB);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_SPEEXWB, value);
        //Opus
        key = resources.getString(R.string.MEDIA_OPUS);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_OPUS, value);

        //dtmf
        key = resources.getString(R.string.MEDIA_DTMF);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_AUDIOCODEC_DTMF, value);
        //h263
        key = resources.getString(R.string.MEDIA_H263);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_VIDEOCODEC_H263, value);
        //h263.98
        key = resources.getString(R.string.MEDIA_H26398);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_VIDEOCODEC_H263_1998, value);
        //h264
        key = resources.getString(R.string.MEDIA_H264);
        value = mPrefrence.getBoolean(key, false);
        codecs.put(PortSipEnumDefine.ENUM_VIDEOCODEC_H264, value);

        return codecs;
    }

    public static UserInfo getUserInfo(Context context) {
        UserInfo infor = new UserInfo();
        SharedPreferences mPrefrence = context
                .getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);
        String stringitem = mPrefrence.getString(PORTSIP_USERNAME, "");
        infor.setUserName(stringitem);
        stringitem = mPrefrence.getString(PORTSIP_USERPASSWORD, "");
        infor.setUserPwd(stringitem);
        stringitem = mPrefrence.getString(PORTSIP_USEDISPALYNAME, "");
        infor.setUserDisplayName(stringitem);
        stringitem = mPrefrence.getString(PORTSIP_SERVER_IP, "");
        infor.setServerIp(stringitem);
        int port  = mPrefrence.getInt(PORTSIP_LOCAL_PORT, 5060);

        infor.setLocalPort(port);
        return infor;
    }

  public static void saveLastCalledNumber(Context context, String number){
      if(number != null && !number.equals("")){
          SharedPreferences mPrefrence = context
                  .getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);
          Editor editor = mPrefrence.edit();
          editor.putString(PORTSIP_LASTDIALEDNUMBER , number);
          editor.commit();
      }
  }
    public static String getLastDialedNumber(Context context){
        SharedPreferences mPrefrence = context
                .getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);
        return mPrefrence.getString(PORTSIP_LASTDIALEDNUMBER , "100");
    }
    public static void setUserInfo(Context context, UserInfo infor) {
        if (infor != null) {
            SharedPreferences mPrefrence = context
                    .getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);
            Editor editor = mPrefrence.edit();
            String stringitem = infor.getUserName();
            editor.putString(PORTSIP_USERNAME, stringitem);

            stringitem = infor.getUserPassword();
            editor.putString(PORTSIP_USERPASSWORD, stringitem);

            stringitem = infor.getUserDisplayName();
            editor.putString(PORTSIP_USEDISPALYNAME, stringitem);

			stringitem = infor.getLocalIp();
			editor.putString(PORTSIP_LOCAL_IP, stringitem);

			int port = infor.getLocalPort();
			editor.putInt(PORTSIP_LOCAL_PORT, port);

			stringitem = infor.getServerIp();
			editor.putString(PORTSIP_SERVER_IP, stringitem);
            editor.commit();
        }
    }


    public static CustomHeaders getCustomHeaders(Context context) {
        CustomHeaders customHeaders = new CustomHeaders();
        SharedPreferences mPrefrence = context
                .getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);
        String stringitem = mPrefrence.getString(PORTSIP_NATIVELANG, "");
        customHeaders.setNativeLanguage(stringitem);
        stringitem = mPrefrence.getString(PORTSIP_FOREIGNLANG, "");
        customHeaders.setForeignLanguage(stringitem);
        stringitem = mPrefrence.getString(PORTSIP_BRDIGEID, "");
        customHeaders.setBridgeId(stringitem);
        stringitem = mPrefrence.getString(PORTSIP_BRDIGECONTACT, "");
        customHeaders.setContactUser(stringitem);
            stringitem = mPrefrence.getString(PORTSIP_GLOBALID, "");
            customHeaders.setGlobalSessionId(stringitem);
        boolean value = mPrefrence.getBoolean(PORTSIP_ISTRANSLATED, true);
        customHeaders.setTranslated(value);
        value = mPrefrence.getBoolean(PORTSIP_STARTINMUTE, false);
        customHeaders.setStartInMute(value);
        value = mPrefrence.getBoolean(PORTSIP_MAINLEG, false);
        customHeaders.setMainLeg(value);
        return customHeaders;
    }

    public static void setCustomHeaders(Context context, CustomHeaders customHeaders) {
        if (customHeaders != null) {
            SharedPreferences mPrefrence = context
                    .getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);
            Editor editor = mPrefrence.edit();
            String stringitem = customHeaders.getNativeLanguage();
            editor.putString(PORTSIP_NATIVELANG, stringitem);
            stringitem = customHeaders.getForeignLanguage();
            editor.putString(PORTSIP_FOREIGNLANG, stringitem);
            stringitem = customHeaders.getBridgeId();
            editor.putString(PORTSIP_BRDIGEID, stringitem);
            stringitem = customHeaders.getContactUser();
            editor.putString(PORTSIP_BRDIGECONTACT, stringitem);
            boolean value = customHeaders.isTranslated();
            editor.putBoolean(PORTSIP_ISTRANSLATED, value);
            value = customHeaders.isStartInMute();
            editor.putBoolean(PORTSIP_STARTINMUTE, value);
            value = customHeaders.isMainLeg();
            editor.putBoolean(PORTSIP_MAINLEG, value);
            stringitem = customHeaders.getGlobalSessionId();
            editor.putString(PORTSIP_GLOBALID, stringitem);
            editor.commit();
        }
    }


    public static void setSrtpType(Context context, int enum_srtpType, PortSipSdk sdk) {
        SharedPreferences mPrefrence = context.getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);
        Editor editor = mPrefrence.edit();
        editor.putInt(PORTSIP_SRTPTYPE, enum_srtpType);
        sdk.setSrtpPolicy(enum_srtpType);
        editor.commit();

    }

    public static int getSrtpType(Context context) {
        SharedPreferences mPrefrence = context.getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);
        return mPrefrence.getInt(PORTSIP_SRTPTYPE, PortSipEnumDefine.ENUM_SRTPPOLICY_NONE);
    }

    public static void setAVArguments(Context context, PortSipSdk sdk) {

        // audio codecs
        SharedPreferences mPrefrence = context.getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);
        sdk.clearAudioCodec();
        Resources resources = context.getResources();

        String key = resources.getString(R.string.MEDIA_G722);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_G722);
        }

        key = resources.getString(R.string.MEDIA_G729);
        if (mPrefrence.getBoolean(key, true)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_G729);
        }

        key = resources.getString(R.string.MEDIA_AMR);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_AMR);
        }

        key = resources.getString(R.string.MEDIA_AMRWB);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_AMRWB);
        }

        key = resources.getString(R.string.MEDIA_GSM);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_GSM);
        }

        key = resources.getString(R.string.MEDIA_PCMA);
        if (mPrefrence.getBoolean(key, true)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_PCMA);
        }

        key = resources.getString(R.string.MEDIA_PCMU);
        if (mPrefrence.getBoolean(key, true)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_PCMU);
        }

        key = resources.getString(R.string.MEDIA_SPEEX);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_SPEEX);
        }

        key = resources.getString(R.string.MEDIA_SPEEXWB);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_SPEEXWB);
        }

        key = resources.getString(R.string.MEDIA_ILBC);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_ILBC);
        }

        key = resources.getString(R.string.MEDIA_ISACWB);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_ISACWB);
        }

        key = resources.getString(R.string.MEDIA_ISACSWB);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_ISACSWB);
        }

        key = resources.getString(R.string.MEDIA_OPUS);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_OPUS);
        }

        key = resources.getString(R.string.MEDIA_DTMF);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_DTMF);
        }


        // Video codecs
        sdk.clearVideoCodec();
        key = resources.getString(R.string.MEDIA_H263);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_H263);
        }
        key = resources.getString(R.string.MEDIA_H26398);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_H263_1998);
        }
        key = resources.getString(R.string.MEDIA_H264);
        if (mPrefrence.getBoolean(key, true)) {
            sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_H264);
        }
        key = resources.getString(R.string.MEDIA_VP8);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_VP8);
        }


        key = resources.getString(R.string.str_resolution);
        sdk.setVideoResolution(Integer.valueOf(mPrefrence.getString(key, "1")));

//		setForward(mpreferences);
        key = resources.getString(R.string.MEDIA_VAD);
        sdk.enableVAD(mPrefrence.getBoolean(key, true));

        key = resources.getString(R.string.MEDIA_AEC);
        sdk.enableAEC(mPrefrence.getBoolean(key, true) ? PortSipEnumDefine.ENUM_EC_DEFAULT : PortSipEnumDefine.ENUM_EC_NONE);

        key = resources.getString(R.string.MEDIA_ANS);
        sdk.enableANS(mPrefrence.getBoolean(key, false) ? PortSipEnumDefine.ENUM_NS_DEFAULT : PortSipEnumDefine.ENUM_NS_NONE);

        key = resources.getString(R.string.MEDIA_AGC);
        sdk.enableAGC(mPrefrence.getBoolean(key, true) ? PortSipEnumDefine.ENUM_AGC_DEFAULT : PortSipEnumDefine.ENUM_AGC_NONE);

        key = resources.getString(R.string.MEDIA_CNG);
        sdk.enableCNG(mPrefrence.getBoolean(key, true));

        key = resources.getString(R.string.str_pracktitle);
        if (mPrefrence.getBoolean(key, false)) {
            sdk.enableReliableProvisional(true);
        }

        // Use earphone
        sdk.setLoudspeakerStatus(false);

        // Use Front Camera
        sdk.setVideoDeviceId(1);
        sdk.setVideoOrientation(PortSipEnumDefine.ENUM_ROTATE_CAPTURE_FRAME_270);
    }
}
