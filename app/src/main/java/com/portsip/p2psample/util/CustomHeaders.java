package com.portsip.p2psample.util;

/**
 * Created by DoronK on 6/16/2015.
 */
public class CustomHeaders {

    public String nativeLanguage;
    public String foreignLanguage;
    public String bridgeId;
    public String contactUser;
    public String globalSessionId;
    public boolean translated , startInMute , mainLeg = false;


    public String getNativeLanguage() {
        return nativeLanguage;
    }

    public void setNativeLanguage(String nativeLanguage) {
        this.nativeLanguage = nativeLanguage;
    }

    public String getForeignLanguage() {
        return foreignLanguage;
    }

    public void setForeignLanguage(String foreignLanguage) {
        this.foreignLanguage = foreignLanguage;
    }

    public String getBridgeId() {
        return bridgeId;
    }

    public void setBridgeId(String bridgeId) {
        this.bridgeId = bridgeId;
    }

    public String getContactUser() {
        return contactUser;
    }

    public void setContactUser(String contactUser) {
        this.contactUser = contactUser;
    }

    public boolean isTranslated() {
        return translated;
    }

    public void setTranslated(boolean translated) {
        this.translated = translated;
    }

    public boolean isStartInMute() {
        return startInMute;
    }

    public void setStartInMute(boolean startInMute) {
        this.startInMute = startInMute;
    }

    public boolean isMainLeg() {
        return mainLeg;
    }

    public void setMainLeg(boolean mainLeg) {
        this.mainLeg = mainLeg;
    }

    public String getGlobalSessionId() {
        return globalSessionId;
    }

    public void setGlobalSessionId(String globalSessionId) {
        this.globalSessionId = globalSessionId;
    }
}
